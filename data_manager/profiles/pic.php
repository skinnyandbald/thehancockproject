<?php
$table = 'tblmedia';

$fields = array (
	array (
		'name' => 'id'
		, 'type' => 'key'
		, 'title' => 'ID'
	)
	, array (
		'name' => 'filename'
		, 'type' => 'input'
		, 'title' => 'First name'
		, 'is_link' => 0
		, 'link' => '/pics/edit/*'
		, 'single_mode' => 0
	)
	, array (
		'name' => 'added_at'
		, 'type' => 'input'
		, 'title' => 'added @'
		, 'is_link' => 0
		, 'link' => '/pics/edit/*'
		, 'single_mode' => 0
	)
	, array (
		'name' => 'published_date'
		, 'type' => 'input'
		, 'title' => 'published @'
		, 'is_link' => 0
		, 'link' => '/pics/edit/*'
	)
	, array (
		'name' => 'managing'
		, 'type' => 'managing'
		, 'title' => 'Actions'
		, 'delete_link' => '/pics/delete/*'
		, 'edit_link' => '/pics/edit/*'
		, 'non_db' => 1
		, 'single_mode' => 0
	)
);

/*
, array (
		'name' => 'footer'
		, 'type' => 'input'
		, 'title' => 'Footer'
		, 'is_link' => 0
		, 'link' => '/edit/*'
	)*/