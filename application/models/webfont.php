<?php

class Webfont extends Font
{
	static public $formats = NULL;
	
	function __construct($public_id = NULL)
	{
		//echo 'Webfont constructor';
		parent::__construct($public_id);
		
		$this->_init();
	}
	
	function _init()
	{
		$this->formats = array('otf','eot','ttf','svg','woff');
	}
	
	function _preview_as_text()
	{
		$alpha 			= str_split('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890,./<>?~!@#$%^&*()_+-=;\':"{}|\';/');
		$extra 			= explode(';', '&#8211;&#8212;&#8217;&#8218;&#8220;&#8221;&deg;');
		$chars 			= array_merge($alpha, $extra);
		
		return $chars;
	}
	
	function preview($format = 'text')
	{
		if($format == 'text')
		{
			$preview = $this->_preview_as_text();
		}
		else
		{
			$preview = parent::preview($format);
		}
		
		return $preview;
	}
	
	function get_src($format = 'otf')
	{
		$paths = new stdClass();
		
		$CI 			=& get_instance();
		
		foreach($this->formats as $f)
		{
			$paths->{$f} = sprintf('%s%s', $CI->config->item('path_to_webfonts'), $this->get_private_filename($f) );
		}
		
		return $paths;
	}
}