<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Font {// extends Model 

	protected $public_id		= NULL;
	protected $email 			= NULL;
	protected $customer_name 	= NULL;
	protected $font_family 		= NULL;
	public $twitter 			= NULL;
	public $font_size			= '1.4em';
	public $line_height			= '1.5em';
	
	
	function __construct($public_id = NULL)
    {
    	//parent::__construct();
    	
    	$this->CI 			=& get_instance();
    	$this->public_id 	= $public_id;
    	
    	if($public_id)
	    	$this->_make_from_id($public_id);
    }
    
    function get_private_id()
    {
	    return $this->email;
    }
    
    function make_from_twitter($twitter)
    {
	    if( $this->public_id == NULL)
	    {
		    $font 					= $this->_get_font_info($twitter, 'remote', 50453997);
		    
		    $this->public_id 		= $font->email;//this will need to be encrypted
		    $this->customer_name 	= $font->customer_name;
		    $this->font_family 		= $font->font_family;
		    $this->font_size		= $font->font_size;
			$this->line_height		= $font->line_height;
	    }
	    
	    $this->twitter 			= $twitter;
    }
    
    function _make_from_id($public_id)
    {
    	//$CI->encrypt->set_cipher(MCRYPT_BLOWFISH);
    	//echo $CI->encrypt->encode($public_id);
    	
	    $this->email 			= $public_id;//$CI->encrypt->decode($public_id);
	    
	    $font 					= $this->_get_font_info($this->email, 'local');
	    $this->customer_name 	= $font->customer_name;
	    $this->font_family 		= $font->font_family;
	    $this->twitter 			= $font->twitter;
	    $this->font_size		= $font->font_size;
	    $this->line_height		= $font->line_height;
    }
    
    function _preview_img()
	{
		echo sprintf('%s%s/%s/text' , base_url(), $this->controller, $this->email);
		die();
		$image_path = url2png( sprintf('%s%s/%s/text' , base_url(), $this->controller, $this->email));//sprintf("%s%s.png", $this->CI->config->item('path_to_png_previews'), $this->email);//this should be consistent w fontfile name... at some point
		return $image_path;
	}
	
	function preview($format = 'image')
	{	
		switch($format)
		{
			case 'image':
				$preview = $this->_preview_img();
				break;
				
			default:
				$webfont = new Webfont($this->public_id);
				$preview = $webfont->preview('text');
		}
		
		return $preview;
	}
    
    function get_public_id()
    {
	    return $this->public_id;
    }
		
	function get_name()
	{
		$font_name = sprintf('%s Hancock', $this->font_family);
		return $font_name;
	}
	
	function get_fontfamily()
	{
		$font_name = str_replace('+', '', str_replace('.', '', str_replace('@', '', $this->get_public_id() )));//sprintf('%s-Hancock', str_replace(" ", "", $this->email));
		return $font_name;
	}
	
	function get_private_filename($ext = 'otf')
	{
		$font_name = sprintf('%s.%s', $this->email, $ext);
		return $font_name;
	}
	
	function get_private_filepath($ext = 'otf')
	{
		
		$path = sprintf('%s%s', $this->CI->config->item('path_to_webfonts'), $this->get_private_filename($ext));
		return $path;
	}
	
	function get_public_filename($ext = 'otf')
	{
		$font_name = sprintf('%s.%s', $this->public_id, $ext);
		return $font_name;
	}
	
	function get_descr()
	{
		$font 					= $this->_get_font_info($this->email, 'remote');
		$this->customer_name 	= $font->customer_name;
		$this->font_family 		= $font->font_family;
		
		$first_name 	=  explode(" ", $this->get_name());
		$first_name 	=  explode("-", $first_name[0]);
		
		$s 				= ( $first_name[0] != '' ? sprintf('%s\'s handwriting', ucwords($first_name[0]) ) : 'The quick, brown fox ate Mr. Hancock');
		
		return $s;
	}
	
	function _get_font_info($search_value, $source = 'local', $search_criteria = 45493357)
	{
		$font_info 	= new stdClass;
		
		if( $source == 'local' || $this->CI->config->item('local_fonts') === TRUE)
		{
			$font_info->customer_name 	= $this->public_id;
			$font_info->font_family 	= $this->public_id;
			$font_info->twitter			= NULL;
			$font_info->font_size 		= $this->font_size;
			$font_info->line_height 	= $this->line_height;
		}
		else
		{
			// Setup the client. See authentication.php
			Podio::setup( $this->CI->config->item('podio_client_id'), $this->CI->config->item('podio_client_secret') );
			Podio::$debug = true;
		
			try
			{
				Podio::authenticate(
									'password', array(
													'username' => $this->CI->config->item('podio_username'),
													'password' => $this->CI->config->item('podio_password')
													)
									);
			
				// Authentication was a success, now you can start making API calls.
				$query 			= array(
									  'filters' => array($search_criteria=>$search_value),
									  'sort_by' => 'created_on'
									  );
							  
				$results 		= PodioItem::filter($this->CI->config->item('podio_app_id'), $query);
				$result_count 	= sizeof($results['items']);
				
				switch($result_count)
				{
					case 0:
						//pre('no results');
						//$font_info->email 			= '';
						$font_info->customer_name 	= '';
						$font_info->font_family 	= '';
						$font_info->twitter			= NULL;
						
						break;
						
					case 1:
						$email 						= PodioItem::get_field_value($results['items'][0]->item_id, 45493357);
						$customer_name 				= PodioItem::get_field_value($results['items'][0]->item_id, 45491648);
						$font_name_override 		= PodioItem::get_field_value($results['items'][0]->item_id, 45167901);
						$twitter 					= PodioItem::get_field_value($results['items'][0]->item_id, 50453997);
						$font_size 					= PodioItem::get_field_value($results['items'][0]->item_id, 50502501);
						$line_height 				= PodioItem::get_field_value($results['items'][0]->item_id, 50502502);
						
						$font_info->email 			= $email[0]['value'];
						$font_info->customer_name 	= $customer_name[0]['value'];
						$font_info->font_family 	= ( sizeof($font_name_override) > 0 ? $font_name_override[0]['value'] : $customer_name[0]['value']);
						$font_info->twitter 		= ( sizeof($twitter) > 0 ? $twitter[0]['value'] : NULL);
						$font_info->font_size 		= ( sizeof($font_size) > 0 ? $font_size[0]['value'] : $this->font_size);
						$font_info->line_height 	= ( sizeof($line_height) > 0 ? $line_height[0]['value'] : $this->line_height);
						
						break;
					
					default:
						pre('too many results');
				}
			}
			catch (PodioError $e)
			{
				// Something went wrong. Examine $e->body['error_description'] for a description of the error.
				pre($e->body['error_description']);
			}
		}
		
		return $font_info;
	}
}