<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * CodeIgniter Inflector Helpers
 *
 * Customised singular and plural helpers.
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team, stensi
 * @link		http://codeigniter.com/user_guide/helpers/inflector_helper.html
 */

// --------------------------------------------------------------------

/**
 * errors
 *
 * Returns all errors
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('errors'))
{
	function errors($p)
	{
		foreach ($p->error->all as $e)
		{
			echo $e . "<br />";
		}
	}
}

/**
 * pre
 *
 * Returns all errors
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('pre'))
{
	function pre($a)
	{
		echo '<pre>';
		print_r($a);
		echo '</pre>';
	}
}
/* End of file path_helper.php */
/* Location: ./application/helpers/inflector_helper.php */