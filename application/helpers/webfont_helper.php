<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * get_webfont_name
 *
 * Returns the font name (if exists) from podio, associated w/ email
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('get_webfont_name'))
{	
	function get_customer_name($email)
	{
		$CI =& get_instance();
		
		// Setup the client. See authentication.php
		Podio::setup( $CI->config->item('podio_client_id'), $CI->config->item('podio_client_secret') );
		Podio::$debug = true;
		
		try
		{
			Podio::authenticate(
								'password', array(
												'username' => $CI->config->item('podio_username'),
												'password' => $CI->config->item('podio_password')
												)
								);
		
			// Authentication was a success, now you can start making API calls.
			$query 			= array(
								  'filters' => array(45493357=>$email),
								  'sort_by' => 'created_on'
								  );
						  
			$results 		= PodioItem::filter($CI->config->item('podio_app_id'), $query);
			$result_count 	= sizeof($results['items']);
			
			switch($result_count)
			{
				case 0:
					return 'no results';
					break;
					
				case 1:
					$item = PodioItem::get_field_value($results['items'][0]->item_id, 45491648);

					return $item[0]['value'];
					break;
				
				default:
					return 'too many results';
			}
		}
		catch (PodioError $e)
		{
			// Something went wrong. Examine $e->body['error_description'] for a description of the error.
			return $e->body['error_description'];
		}
	}
	
	function get_webfont_filename($email, $len = 8) {
	    return $this->encrypt->encode($email);
	}
}

/* End of file path_helper.php */
/* Location: ./application/helpers/inflector_helper.php */