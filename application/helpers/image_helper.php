<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------

/**
 * get_webfont_name
 *
 * Returns the font name (if exists) from podio, associated w/ email
 *
 * @access	public
 * @return	string
 */
if ( ! function_exists('url2png'))
{	
	function url2png($url)//, $args
	{
		$CI =& get_instance();
		
		$URL2PNG_APIKEY 	= $CI->config->item('key', 'url2png');
		$URL2PNG_SECRET 	= $CI->config->item('secret', 'url2png');
		
		# urlencode request target
		$options['url'] 		= urlencode($url);
		
		$options += $CI->config->item('args', 'url2png');
		
		# create the query string based on the options
		foreach($options as $key => $value) { $_parts[] = "$key=$value"; }
		
		# create a token from the ENTIRE query string
		$query_string = implode("&", $_parts);
		$TOKEN = md5($query_string . $URL2PNG_SECRET);
		
		return "http://beta.url2png.com/v6/$URL2PNG_APIKEY/$TOKEN/png/?$query_string";
	}
}

/* End of file path_helper.php */
/* Location: ./application/helpers/inflector_helper.php */