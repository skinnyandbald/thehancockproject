<?php
$data['page_title'] 	= $this->config->item('default_page_title');

$this->load->view("common/header", $data);
$this->load->view($view);
$this->load->view("common/footer", $data);