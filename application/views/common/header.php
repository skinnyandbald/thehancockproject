<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->

<head>
	<meta charset="utf-8">
	<title><?php echo $page_title ?></title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- | required styles | -->
	<link rel="stylesheet" href="<?php echo asset_url() ?>css/normalize.css">
	<link rel="stylesheet" href="<?php echo asset_url() ?>css/foundation.css">
	<link rel="stylesheet" href="<?php echo asset_url() ?>css/style.css">
	
	<!-- | custom styles | -->
	<link rel="stylesheet" href="<?php echo asset_url() ?>css/general_enclosed_foundicons.css">
	<link rel="stylesheet" href="<?php echo asset_url() ?>css/custom_base.php?asset_url=<?php echo urlencode(asset_url()) ?>" type='text/css'>
	
	<?php $custom_style =  sprintf('css/custom_%s.php', $controller); ?>
	<?php if( file_exists( sprintf('assets/%s', $custom_style))): ?>
		<link rel="stylesheet" href="<?php echo sprintf( '%s%s?asset_url=%s', asset_url(), $custom_style, urlencode(asset_url())) ?>" type='text/css'>
	<?php endif; ?>
	
	<link href='<?php echo sprintf('%sapi/fontface/%s', base_url(), 'kim@zinepak.com' ) ?>' rel='stylesheet' type='text/css'>
	<link href='<?php echo sprintf('%sapi/fontface/%s', base_url(), 'jacobyr@gmail.com' ) ?>' rel='stylesheet' type='text/css'>
	
	<?php if( isset($fonts) ): ?>
		<?php foreach( $fonts as $f ): ?>
			<link href='<?php echo sprintf('%sapi/fontface/%s', base_url(), $f->get_public_id() ) ?>' rel='stylesheet' type='text/css'>
		<?php endforeach; ?>
	<?php endif; ?>
	
	<!-- | required JS | -->
	<script src="<?php echo asset_url() ?>js/vendor/custom.modernizr.js"></script>
	
	<!-- | enforce webfont smoothing | -->
	<script src="<?php echo asset_url() ?>js/vendor/fontsmoothie.min.js"></script>

</head>
<body>

<?php if( ! isset($no_nav) || $no_nav === FALSE ): ?>
	<!-- Navigation --> 
	<nav class="top-bar" data-topbar>
		<div class="row header">
			<div class="large-6 small-2 columns brand-face"></div>
			<div class="large-6 small-10 columns text-right hot brand-text">
				THE HANCOCK PROJECT
			</div>
		</div>
	</nav>
	<!-- End Nav -->
<?php endif; ?>