<!-- Footer -->
<?php if( ! isset($no_footer)): ?>
	<footer class="row">
		<div class="large-12 columns">
			<hr>
			<div class="row">
				<div class="large-6 columns">
					<p>
						&copy; THE HANCOCK PROJECT 2013
					</p>
				</div>
			</div>
		</div>
	</footer>
<?php endif; ?>
<!-- End Footer -->

<script src="<?php echo asset_url() ?>js/vendor/jquery.js"></script>
<script src="<?php echo asset_url() ?>js/foundation.min.js"></script>
<script>
	$(document).foundation();
</script>

<!-- | custom JS | -->
<script src="<?php echo asset_url() ?>js/custom.js"></script>

<?php if($controller == 'toy' && isset($fonts)): ?>
	<script src="<?php echo asset_url() ?>js/twitter_font.php?fontfamily=<?php echo $fonts[0]->get_fontfamily() ?>&fontface_path=<?php echo urlencode( sprintf('%sapi/fontface/%s', base_url(), $fonts[0]->get_public_id() )) ?>&font_size=<?php echo $fonts[0]->font_size; ?>&line_height=<?php echo $fonts[0]->line_height; ?>"></script>
<?php endif; ?>

<?php if( isset($payment)): ?>
	<script src="https://checkout.stripe.com/checkout.js"></script>
	<script>
		 var handler = StripeCheckout.configure({
		    key: 				'<?php echo $this->config->item('stripe_pk') ?>',
		    email: 				'ben@skinnyandbald.com',
		    billingAddress: 	false,
		    shippingAddress: 	false,
		    amount:      		$(this).data('amount'),
		    name:        		'THE HANCOCK PROJECT',
		    description:		$(this).data('desc'),
		    panelLabel:  		'Checkout',
		    image: 				'<?php echo asset_url() ?>images/hancock-logo.png',
		    token: function(token, args) {
		      var $stripeToken 	= $('<input type=hidden name=stripeToken />').val(token.id);
			  var $stripeEmail 	= $('<input type=hidden name=stripeEmail />').val(token.email);
				
			  $('form').append($stripeToken).append($stripeEmail).submit();
		    }
		  });
		  
		  $('button.buy').click(function(e){
			    
			    // Open Checkout with further options
			    handler.open({
			    });
			    
			$('#amount').val( $(this).data('amount') );
			$('#desc').val( $(this).data('desc') );
			e.preventDefault();
		  });
	</script>
<?php endif; ?>

<?php if( isset($olark)): ?>
	<!-- begin olark code -->
	<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark||(function(c){var f=window,d=document,l=f.location.protocol=="https:"?"https:":"http:",z=c.name,r="load";var nt=function(){
	f[z]=function(){
	(a.s=a.s||[]).push(arguments)};var a=f[z]._={
	},q=c.methods.length;while(q--){(function(n){f[z][n]=function(){
	f[z]("call",n,arguments)}})(c.methods[q])}a.l=c.loader;a.i=nt;a.p={
	0:+new Date};a.P=function(u){
	a.p[u]=new Date-a.p[0]};function s(){
	a.P(r);f[z](r)}f.addEventListener?f.addEventListener(r,s,false):f.attachEvent("on"+r,s);var ld=function(){function p(hd){
	hd="head";return["<",hd,"></",hd,"><",i,' onl' + 'oad="var d=',g,";d.getElementsByTagName('head')[0].",j,"(d.",h,"('script')).",k,"='",l,"//",a.l,"'",'"',"></",i,">"].join("")}var i="body",m=d[i];if(!m){
	return setTimeout(ld,100)}a.P(1);var j="appendChild",h="createElement",k="src",n=d[h]("div"),v=n[j](d[h](z)),b=d[h]("iframe"),g="document",e="domain",o;n.style.display="none";m.insertBefore(n,m.firstChild).id=z;b.frameBorder="0";b.id=z+"-loader";if(/MSIE[ ]+6/.test(navigator.userAgent)){
	b.src="javascript:false"}b.allowTransparency="true";v[j](b);try{
	b.contentWindow[g].open()}catch(w){
	c[e]=d[e];o="javascript:var d="+g+".open();d.domain='"+d.domain+"';";b[k]=o+"void(0);"}try{
	var t=b.contentWindow[g];t.write(p());t.close()}catch(x){
	b[k]=o+'d.write("'+p().replace(/"/g,String.fromCharCode(92)+'"')+'");d.close();'}a.P(2)};ld()};nt()})({
	loader: "static.olark.com/jsclient/loader0.js",name:"olark",methods:["configure","extend","declare","identify"]});
	/* custom configuration goes here (www.olark.com/documentation) */
	olark.identify('7189-594-10-3589');/*]]>*/</script><noscript><a href="https://www.olark.com/site/7189-594-10-3589/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>
	<!-- end olark code -->
<?php endif; ?>

<!-- End Footer -->

</body>
</html>
