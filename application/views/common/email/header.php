<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width"/>
  <link rel="stylesheet" href="<?php echo asset_url() ?>css/ink.css">
  
  <link href='http://skinnyandbald.com/fonts/css.php?family=ben@skinnyandbald.com' rel='stylesheet' type='text/css'>
  
  <style>
  	.right img {
	  	float: right;
  	}
  
  	.hot {
	    color:#f30e8f !important;
    }
    
    .step-label {
	    font-weight:lighter;
	    color: #838383;
    }
    
    .facebook table td {
      background: #3b5998;
      border-color: #2d4473;
    }

    .facebook:hover table td {
      background: #2d4473 !important;
    }

    .twitter table td {
      background: #00acee;
      border-color: #0087bb;
    }

    .twitter:hover table td {
      background: #0087bb !important;
    }

    .google-plus table td {
      background-color: #DB4A39;
      border-color: #CC0000;
    }

    .google-plus:hover table td {
      background: #CC0000 !important;
    }

    .template-label {
      color: #ffffff;
      font-weight: bold;
      font-size: 11px;
    }

    .callout .panel {
      background: #ECF8FF;
      border-color: #b9e5ff;
    }

    .header {
     	background: url('<?php echo asset_url() ?>images/email/bg.gif') repeat-x #282828;
     	background-size: 50%;
     	max-height: 70px;
     	height:auto;
    }

    .footer .wrapper {
      background: #ebebeb;
    }

    .footer h5 {
      padding-bottom: 10px;
    }

    table.columns .text-pad {
      padding-left: 10px;
      padding-right: 10px;
    }

    table.columns .left-text-pad {
      padding-left: 10px;
    }

    table.columns .right-text-pad {
      padding-right: 10px;
    }

    @media only screen and (max-width: 600px) {

      table[class="body"] .right-text-pad {
        padding-left: 10px !important;
      }

      table[class="body"] .left-text-pad {
        padding-right: 10px !important;
      }
    }
    
    .centered {
	  	text-align: center !important;
  	}

  </style>
</head>
<body class="">
  <table class="body">
    <tr>
      <td class="center" align="center" valign="top">
        <center>