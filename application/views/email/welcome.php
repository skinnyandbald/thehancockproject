<table class="row header">
	<tr>
		<td align="center" class="center">
			<table class="container">
				<tr>
					<td class="wrapper last">
						<table class="twelve columns">
							<tr>
								<td class="six sub-columns"><img src="<?php echo asset_url() ?>images/email/hancock.png" style="height:auto;max-height:50px;max-width:50px;width:100%;"></td>

								<td class="six sub-columns last" style="text-align:right; vertical-align:middle;"><span class="template-label hot">THE HANCOCK PROJECT</span></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br>

<table class="container">
	<tr>
		<td>
			<!-- content start -->

			<table class="row">
				<tr>
					<td class="wrapper last">
						<table class="twelve columns">
							<tr>
								<td>
									<h3>Wooo! Thanks for the money <span class="hot" style="font-size:.8em">=)</span></h3>
									<br>
									<h6>
										And here's a glimpse of what *you* have to look forward to&mdash;
									</h6>
									
									<br>
									
									<a href="http://iamtravisnichols.com" target=_blank><img src="http://thehancockproject.com/preview/parchment.php?hancock=travis%2B2%40gumroad.com" width="580" alt="Travis is a NYC-based author & illustrator" border="0"></a>
								</td>

								<td class="expander"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<br>
			<table class="row">
				<tr>
					<td class="wrapper last">
						<table class="twelve columns">
							<tr>
								<td>
									<h6>
										<span style="font-size:2em">But, first things first.</span><br/><br/>
										We need a specimen of your handwriting to <span class="hot">fontifize</span>.<br/>
										This should only take ~5 mins. 
									</h6>
								</td>

								<td class="expander"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			
			<br>
			<table class="row">
				<tr>
					<td class="wrapper">
						<table class="twelve columns">
							<tr>
								<td class="three sub-columns right"><!-- Top Left sub-grid -->
								<img height="100" src="<?php echo asset_url() ?>images/email/step1.png" width="100"></td>

								<td class="nine sub-columns last">
									<!-- Top Right sub-grid -->

									<h5><span class="step-label">1.</span> Print the *sacred* template</h5>
									
									<br>

									<table width="250">
										<tr>
											<td>
												<a class="button" href="#">
												<table>
													<tr>
														<td>Click to download &raquo;</td>
													</tr>
												</table></a>
											</td>
										</tr>
									</table>
								</td>

								<td class="expander"></td>
							</tr>
						</table>

						<table class="twelve columns">
							<tr>
								<td class="three sub-columns right"><!-- Top Left sub-grid -->
								<img height="100" src="<?php echo asset_url() ?>images/email/step2.png" width="100"></td>

								<td class="nine sub-columns last">
									<!-- Top Right sub-grid -->

									<h5><span class="step-label">2.</span> Handwrite your alphabet</h5>
									<p style="font-size:.9em; color:#878787;">Just like your 3rd grade teacher taught ya.</p>
								</td>

								<td class="expander"></td>
							</tr>
						</table>
						
						<table class="twelve columns">
							<tr>
								<td class="three sub-columns right">
								<img height="100" src="<?php echo asset_url() ?>images/email/step3.png" width="100"></td>

								<td class="nine sub-columns last">
									<h5><span class="step-label">3.</span> Send us a scan</h5>

									<p style="font-size:.9em; color:#878787;">by hitting <span style="color:#ee4387">"Reply"</span>.</p>
									
									<p style="font-size:.9em; color:#878787;">
										If you don't have a scanner, send a <span style="color:#ee4387">good</span> overhead photo.<br>(<a href="" target="_blank">See example</a>)</p>
									</p>
								</td>

								<td class="expander"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><!-- Panel with call to action -->

			<table class="row callout">
				<tr>
					<td class="wrapper last">
						<table class="twelve columns">
							<tr>
								<td class="panel">
									<center>Still got questions? Check the <a href="http://theHancockProject.com/app/faq">FAQ »</a></center>
								</td>

								<td class="expander"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><br>

			<table class="row footer">
				<tr>
					<td class="wrapper">
						<table class="three columns">
							<tr>
								<td class="left-text-pad">
									<a class="tiny-button facebook" href="#">
									<table>
										<tr>
											<td>Facebook</td>
										</tr>
									</table></a><br>
									<a class="tiny-button twitter" href="#">
									<table>
										<tr>
											<td>Twitter</td>
										</tr>
									</table></a>
								</td>

								<td class="expander"></td>
							</tr>
						</table>
					</td>

					<td class="wrapper last">
						<table class="nine columns">
							<tr>
								<td class="last right-text-pad">
									<h5>THE HANCOCK BOYS</h5>

									<p>919.357.3369</p>

									<p><a href="mailto:john@thehancockproject.com">john@thehancockproject.com</a></p>
								</td>

								<td class="expander"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="row">
				<tr>
					<td class="wrapper last">
						<table class="twelve columns">
							<tr>
								<td align="center">
									<p style="text-align:center;"><a href="#">Terms</a> | <a href="#">Privacy</a> | <a href="#">Unsubscribe</a></p><br>
								</td>

								<td class="expander"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table><!-- container end below -->
		</td>
	</tr>
</table>

