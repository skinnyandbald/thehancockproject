<div class="row" id="<?php echo $id ?>" style="padding:0 1em;">
	<div class="row text-center">
		<h2 style="margin-top:1em; margin-bottom:0em;">We'll turn <span class="hot">*</span>YOUR<span class="hot">*</span> handwriting into a <span class="hot">*</span>FONT<span class="hot">*</span></h2><br />

		<div class="large-12 columns">
			<div class="large-4 columns text-right" style="padding-top:2em;">
				<h5>This year, give a gift worth<br />
				writing home about!<br />
				<br />
				<span class="hot">A one-a-kind font<br />
				of <em>their</em> handwriting.</span><br />
				<br />
				It's the perfect gift or stocking stuffer for anyone with—or without—hands!</h5>
			</div>

			<div class="large-8 columns">
				<ul data-options="animation_speed:500; timer_speed:2000; bullets:false; timer: true; variable_height:false; swipe:true; pause_on_hover: true; resume_on_mouseout:true; caption_class:orbit-caption; arrows" data-orbit="" id="slider">
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('travis+2@gumroad.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('greta@luciddesign.co.nz')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('kim@zinepak.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('rydel')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('john.marbach@gmail.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('shawna.kaufman@gmail.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('drmarge@earthlink.net')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('brittany@zinepak.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('dorothyjean@gmail.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('eric.noeth@gmail.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('jonathanlimes@gmail.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('timhofmann@gmail.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('masha.rikhter@gmail.com')) ?>" />
					</li>
					<li>
						<img src="<?php echo sprintf('%sapi/preview/%s/image', base_url(), urlencode('sljh4j@hotmail.com')) ?>" />
					</li>
				</ul>
			</div>
		</div>
	</div><br />
	<hr />

	<div class="row">
		<div class="large-12">
			<h4>Features</h4>

			<ul class="panel">
				<li>Our patent-pending template for capturing a person's handwriting sample</li>

				<li>72-hour turnaround</li>

				<li>Make digital communication more personal - websites, blogs, social networks</li>

				<li>Make daily life more interesting and fun - spreadsheets, address labels, writing</li>
			</ul>
		</div>
	</div>
	<hr />
	<br />

	<div class="row">
		<div class="large-12">
			<p>Giving a Hancock font is a fun gift that is out of the ordinary and works well last-minute!</p>

			<p>Oh, and did we mention satisfaction is guaranteed?</p>

			<p>So what are you waiting for?! Gift one to your roommate, your mom, your crush, your crazy aunt, or all of them?</p>
		</div>
	</div>
	<hr />

	<div class="row">
		<div class="large-12">
			<h4>Deliver your gift how you'd like! <small>(Slay bells *not* included)</small></h4>

			<ul class="panel callout">
				<li><input name="delivery" type="radio" /> <span class="label radius">Email</span> (We send it on your behalf right away!)</li>

				<li><input name="delivery" type="radio" /> <span class="label radius">Print it yourself</span> ('Nuff said. Pairs well with a card, stocking or... a bottle of booze!)</li>

				<li><input name="delivery" type="radio" /> <span class="label radius">Santa</span> (We'll send your gift from the North Pole, to maximize the magical experience.)</li>
			</ul>

			<p><button class="button buy" data-amount="19.95" data-desc="Hand =&gt; Font">Buy it Now! (<span style="text-decoration: line-through">$19.95</span> $9.95)</button></p>
		</div>
	</div>
</div>