<style>
	<?php foreach($fonts as $f): ?>
  		.<?php echo $f->get_fontfamily() ?> {
  			font-family: '<?php echo $f->get_fontfamily() ?>';
  		}
  	<?php endforeach; ?>
</style>

<div class="row" id="<?php echo $id ?>">
	<?php foreach($fonts as $f): ?>
		<div class="row" style="">
			<div class="overlay <?php echo $f->get_fontfamily() ?>">
				<?php foreach($f->preview('text') as $c): ?>
					<div class="large-1 small-1 columns"><?php echo $c ?></div>
				<?php endforeach; ?>
				
				<div class="large-12 small-12 right hot tag desc"><?php echo $f->get_descr() ?></div>
			</div>
		</div>
	<?php endforeach; ?>
</div>