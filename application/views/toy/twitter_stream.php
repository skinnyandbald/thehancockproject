<?php
	$font = $fonts[0];
?>
<style>
	h4, h4 a, h4 a:active, h4 a:visited, h4 a:link {
		font-family: '<?php echo $font->get_fontfamily() ?>';
	}
</style>

<div class="large-6 large-centered small-12 columns handmade_logo"></div>

<div class="large-6 large-centered small-12 columns">
	<h4><span class="hot">@</span><a href="https://twitter.com/<?php echo $font->twitter ?>" target=_blank><?php echo $font->twitter ?></a></h4>
</div>

<div class="large-6 large-centered small-12 small-centered columns" style="width:520px;">
	<a class="twitter-timeline" href="https://twitter.com/<?php echo $font->twitter ?>" data-screen-name="<?php echo $font->twitter ?>" data-show-replies="FALSE" data-chrome="transparent noheader noscrollbar" data-link-color="#ec168c" data-widget-id="410350070786060288" width="520" ><!-- Tweets by @<?php echo $font->twitter ?> --></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>