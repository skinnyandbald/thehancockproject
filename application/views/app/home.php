<form action="<?php echo site_url($controller) ?>/charge" method="post">

	<input type="hidden" name="amount" value="19.95" id="amount" />
	<input type="hidden" name="desc" id="desc" />
	
	<div class="large-12 columns">
		<!-- Content Slider -->
		<div class="row">
			<div class="large-12">
				<div id="featured">
					<img src="<?php echo asset_url() ?>images/hero-2.png" alt="" >
				</div>
			</div>
		</div>
		
		<div class="row press" style="margin-bottom:2em;">
			As seen on <img src="<?php echo asset_url() ?>images/lifehacker-logo.png" class="lifehacker" />
		</div>
		
		<!-- End Content Slider -->
		<div class="row">
			<div class="large-7 columns">
				<h4>A font as unique as you.</h4>
				<div class="row">
					<div class="large-12 columns">
						<p>
							In our modern digital age, it seems like we are losing those personal touches of yore. We thought it would be great to bring some personality back and have fun doing it. That's how The Hancock Project was born.
						</p>
						
						<p>We transform a sample of your handwriting into a custom font.</p>
						
						<p>
							<button class="button buy" data-amount="19.95" data-desc="Hand =&gt; Font">Buy Now! ($19.95)</button>
						</p>
					</div>
				</div>
				
				<hr style="" />
		
				<p style="margin:2em 0em 2.25em 0em;">
					<img src="<?php echo asset_url() ?>images/quote-dinorah.png">
				</p>
				
				<hr style="" />
			</div>
			
			<div class="large-5 columns">
				<img src="<?php echo asset_url() ?>images/empire.png">
			</div>
		</div>
	
		<div class="row" style="margin-top:1em;">
			<div class="large-6 columns">
				<h4>Extend the reach of your personal touch.</h4>
				<ul>
			        <li>Make spreadsheets and everyday documents more fun, using your own handwriting.</li>
			        <li>Musician? Show off your lyrical prowess in strokes that match the feelings that went into each song.</li>
			        <li>Feel like you don't send physical mail as much as you'd like? Try printing your handwriting for a time-saving way to write more letters.</li>
				    <li>For that added personal touch, you can even print address labels - one of our customers showed us this great use of their font!</li>
				</ul>
			</div>
			
			<div class="large-6 columns">
				<h4>Lifehacks, with your own font!</h4>
				<ul>
					<li>Modify any website font to be your own handwriting with <a href='http://lifehacker.com/5830638/tweak-your-favorite-web-sites-css-with-stylebotLooking' target='_blank'>Stylebot</a>.</li>
					<li>Customize your <a href='http://lifehacker.com/5890139/how-to-create-a-custom-theme-for-your-wordpress-blog-with-minimal-coding-required' target='_blank'>wordpress theme</a> to use your brand new font!
					<li>If you're really into tinkering, try your handwriting font on your <a href='http://lifehacker.com/5991283/how-to-customize-your-iphones-home-screen-and-break-away-from-the-pack' target='_blank'>iPhone</a>, <a href='http://lifehacker.com/how-to-create-your-own-customized-version-of-android-wi-1440101209' target='_blank'>Android</a>, or <a href='http://lifehacker.com/5918036/install-custom-fonts-on-a-hacked-kindle' target='_blank'>kindle</a>.
				</ul>
			</div>
		</div>
	
		<div class="row" style="margin-top:0em;">
			<div class="large-12 columns">
				<p>
					<img src="<?php echo asset_url() ?>images/how.png">
				</p>
			</div>
		</div>
		
		<div class="row" style="margin-top:0em;">
			<div class="large-12 columns">
				<hr style="" />
				
				<p align="center">
					<img src="<?php echo asset_url() ?>images/quote-michael.png" style="max-height:120px;">
				</p>
				
				<hr style="" />
			</div>
		</div>
		
		<div class="row">
			<p align="center" style="margin-top:2.5em;">
				<button class="button buy" data-amount="19.95" data-desc="Hand =&gt; Font">Buy Yours! ($19.95)</button>
			</p>
		</div>
	<!-- End Content -->

</form>