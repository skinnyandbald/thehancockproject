<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Profiler Sections
| -------------------------------------------------------------------------
| This file lets you determine whether or not various sections of Profiler
| data are displayed when the Profiler is enabled.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/profiling.html
|
*/

$stripe['test']['public']	= 'pk_test_907ALHpRuXRmUKldK5a6SZud';
$stripe['test']['secret']	= 'sk_test_GYeV1mwbqmOAhzFVlvu7XzMW';
$stripe['live']['public']	= 'pk_live_6jstmtzblgDLDJf0KK0Aotnz';
$stripe['live']['secret']	= 'sk_live_Rwv6kovpDXsusPvLNOe2hLvy';

$config['stripe_test_mode']	= TRUE;

$mode = ($config['stripe_test_mode'] == TRUE ? 'test' : 'live');

$config['stripe_pk'] = $stripe[$mode]['public'];
$config['stripe_sk'] = $stripe[$mode]['secret'];

/* End of file profiler.php */
/* Location: ./application/config/profiler.php */