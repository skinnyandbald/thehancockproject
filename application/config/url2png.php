<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| Kissmetrics
| -------------------------------------------------------------------
*/

$config['url2png']['key'] 			= 'P52C24924DE603';
$config['url2png']['secret'] 		= 'S57E9F7132BB85';

# usage
$config['url2png']['args']['force']     			= 'false';      # [false,always,timestamp] Default: false
$config['url2png']['args']['fullpage']  			= 'false';      # [true,false] Default: false
$config['url2png']['args']['thumbnail_max_width'] 	= 'false';      # scaled image width in pixels; Default no-scaling.
$config['url2png']['args']['viewport']  			= "800x449";  # Max 5000x5000; Default 1280x1024

/* End of file database.php */
/* Location: ./application/config/database.php */