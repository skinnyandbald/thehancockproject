<?php
class Land extends MY_Controller
{
	private $controller = '';

	function __construct()
	{
		parent::__construct();
		
		$this->controller = strtolower(__CLASS__);
	}
	
	function index(){
	}
	
	function gift()
	{
		$data = array('template' => 'gift');
		$this->_load($data);
	}
	
	function r5()
	{
		$data = array('template' => 'r5');
		$this->_load($data);
	}
	
	function address_labels()
	{
		$data = array('template' => 'address_labels');
		$this->_load($data);
	}
	
	function _load($data)
	{
		$data['id']			= $data['template'];
		$data['payment']	= TRUE;
		$data['controller'] = $this->controller;
		$data['view'] 		= $this->controller.'/'.$data['template'];
		$this->load->view('common/skeleton', $data);
	}
}