<?php
class App extends MY_Controller
{
	private $controller = '';

	function __construct()
	{
		parent::__construct();
		
		$this->controller = strtolower(__CLASS__);
	}
	
	function index()
	{
		$data['id'] 		= 'home';
		$data['payment']	= TRUE;
		$data['controller'] = $this->controller;
		$data['view'] 		= $this->controller.'/home';
		$this->load->view('common/skeleton', $data);
	}
		
	function faq()
	{
		$data['id'] 		= $this->uri->segment(2);
		$data['controller'] = $this->controller;
		$data['view'] 		= $this->controller.'/faq';
		$this->load->view('common/skeleton', $data);
	}
	
	function terms()
	{
		$data['id'] 		= $this->uri->segment(2);
		$data['controller'] = $this->controller;
		$data['view'] 		= $this->controller.'/terms';
		$this->load->view('common/skeleton', $data);
	}
	
	function services()
	{
		$data['id'] 		= $this->uri->segment(2);
		$data['controller'] = $this->controller;
		$data['view'] 		= $this->controller.'/services';
		$this->load->view('common/skeleton', $data);
	}
	
	function charge()
	{
		if( isset($_POST['stripeToken']) )
			$this->_charge();
	}
	
	function _charge()
	{
		$token  		= $_POST['stripeToken'];
		$amount  		= $_POST['amount'];
		$desc			= $_POST['desc'];
		$email			= $_POST['stripeEmail'];
		
		$params_cust 	= array(
			  					'desc'			=> $email,
			  					'email'			=> $email,
			  					'identifier' 	=> $token
			  				);
		
		$response_cust	= $this->payments->customer_create('stripe', $params_cust);
		
		
		$params_ch 		= array(
								'identifier' 	=> $response_cust->details->identifier,
								'currency_code'	=> 'usd',
				  				'amt'			=> $amount,
				  				'desc'			=> $desc
			  				);
		
		$response_ch	= $this->payments->customer_charge('stripe', $params_ch);
		
		$data['id'] 		= $this->uri->segment(2);
		$data['controller'] = $this->controller;
		$data['view'] 		= $this->controller.'/success';
		$this->load->view('common/skeleton', $data);
	}
	
	function success()
	{
		$data['id'] 		= $this->uri->segment(2);
		$data['controller'] = $this->controller;
		$data['view'] 		= $this->controller.'/success';
		$this->load->view('common/skeleton', $data);
	}
}