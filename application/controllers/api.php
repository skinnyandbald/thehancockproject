<?php
class Api extends MY_Controller
{
	private $controller = '';
	
	function __construct()
	{
		parent::__construct();
		
		$this->controller = strtolower(__CLASS__);
	}
	
	function preview($public_id, $format = 'image')
	{
		$fonts 	= array();
		$ids 	= (is_array($public_id) ? $public_id : array($public_id) );
		
		foreach($ids as $i)
		{
			$fonts[] = ($format == 'text' ? new Webfont($i) : new Font($i));
		}
		
		$data 				= array(
									'id'			=> 'webfont_preview',
									'controller'	=> $this->controller,
									'fonts'			=> $fonts,
									'page_title'	=> $fonts[0]->get_descr(),
									'no_nav'		=> ($format == 'image' ? TRUE : TRUE),
									'no_footer'		=> ($format == 'image' ? TRUE : TRUE),
									);
		
		switch($format)
		{
			case 'image':
				$this->_preview_image($data);
				break;
			
			case 'text':
				$this->_preview_text($data);
		}
	}
	
	function _preview_text($data)
	{
		$data['view'] = $this->controller.'/preview_text';
		$this->load->view('common/skeleton', $data);
	}
	
	function _preview_image($data)
	{
		$this->load->view($this->controller.'/preview_image', $data);
	}
	
	function preview_all($format = 'text')
	{
		$ids 	= array();
		$files 	= glob('/ben/Dropbox/1 proj/the-hancock-project/# orders/8 web font (font)/*.{woff}', GLOB_BRACE);
		
		foreach($files as $f)
		{
			$explode 	= explode('/', $f);
			$ids[] 		= str_replace('.woff', '', $explode[ sizeof($explode)-1 ]);
		}
		
		return $this->preview($ids, $format);
	}
	
	function fontface($public_id)
	{
		$data['webfont'] = new Webfont($public_id);
		$this->load->view($this->controller.'/fontface_css', $data);
	}
}