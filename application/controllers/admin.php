<?php
class Admin extends MY_Controller
{
	private $controller = '';

	function __construct()
	{
		parent::__construct();
		
		$this->controller = strtolower(__CLASS__);
	}
	
	function index()
	{
		
	}
	
	function preview($template = 'welcome')
	{
		$data['controller'] = $this->controller;
		$data['sub'] 		= $this->controller;
		$data['view'] 		= $this->controller.'/'.$template;
		$this->load->view('common/skeleton', $data);
	}
	
	function webfont()
	{
		$response = Unirest::post(
			"https://ofc.p.mashape.com/directConvert/",
			array(
				"X-Mashape-Authorization" => "GPqiwhnOUEhiwHdGkuRH8pvkL1wuYm5S"
			),
			array(
				"file" => "@/tmp/file.path",
				"format" => "woff"
				)
		);
	}
	
	function webfont_callback()
	{
		pre($_FILES);
		
		pre($_POST);
	}
	
	function unirest_test()
	{
		
		$response = Unirest::post("http://httpbin.org/post", array( "Accept" => "application/json" ),
			array(
				"parameter" => 23,
				"foo" 		=> "bar"
			)
		);
		
		$response->code; // HTTP Status code
		$response->headers; // Headers
		$response->body; // Parsed body
		$response->raw_body; // Unparsed body
	}
	
	function send()
	{
		/*
		$response = Unirest::post(
								"https://ofc.p.mashape.com/directConvert/",
									array(
										"X-Mashape-Authorization" => "GPqiwhnOUEhiwHdGkuRH8pvkL1wuYm5S"
									),
									array(
										"file" => "@/ben/AaronRoss-Hancock.otf",
										"format" => "ttf"
									)
								);
		*/
		
		$response = Unirest::post(
								"https://ofc.p.mashape.com/directConvert/",
									array(
										"X-Mashape-Authorization" => "GPqiwhnOUEhiwHdGkuRH8pvkL1wuYm5S"
									),
									array(
										"file" => "@/ben/AaronRoss-Hancock.otf",
										"format" => "ttf",
									)
								);
								
		//header('Content-Type: ' . $response->headers['content-type']);
		//header('Content-Disposition: ' . $response->headers['content-disposition']);
		//echo $response->body;
		
		//pre($response);
		
		$temp = tempnam(sys_get_temp_dir(), 'Tux');
		
		$handle = fopen($temp, "w");
		fwrite($handle, $response->body);
		fseek($handle, 0);
		//fclose($handle);
		
		try {
			echo $temp;
			$phar = new PharData($temp);
			$phar->extractTo('/ben');
			PharData::extractTo('/ben');
			/*if( isset($res) ) {
		        file_put_contents( $des, $res )    ;
		        echo $des . '  Created <br />'     ;
		      }*/
		} catch (Exception $e) {
		    // handle errors
		    pre($e);
		}
		
		//fclose($handle); // this removes the file
	}
	
	function receive()
	{
		// sample script that only prints the POST result
		
		print_r($_FILES);
		/*
		Outputs:
		 Array
		 (
		    [file] => Array
		        (
		            [name] => Geogrotesque-SemiBold.pfb
		            [type] => application/octet-stream
		            [tmp_name] => /tmp/phpRHF74I
		            [error] => 0
		            [size] => 42816
		        )
		
		 )
		*/
		
		print_r($_GET);/*
		Outputs:
		Array
		(
		)
		*/
		
		print_r($_POST);
		/*
		Outputs:
		Array
		(
		   [originalFileName] => style_169898.ttf
		)
		*/

	}
}