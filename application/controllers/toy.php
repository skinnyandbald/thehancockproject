<?php
class Toy extends MY_Controller
{
	private $controller = '';
	
	function __construct()
	{
		parent::__construct();
		
		$this->controller = strtolower(__CLASS__);
		$this->load->helper('webfont');
	}
	
	function twitter($twitter = 'skinnyandbald', $public_id = NULL)//,
	{
		$webfont 	= new Webfont($public_id);
		$webfont->make_from_twitter($twitter);
		
		$data 		= array(
							'fonts'			=> array($webfont),
							'page_title'	=> $webfont->get_name(),
							'no_nav'		=> TRUE,
							'no_footer'		=> TRUE
							);
		
		$data['id'] 		= $this->controller;
		$data['controller'] = $this->controller;
		$data['data']		= $data;
		$data['view'] 		= $this->controller.'/twitter_stream';
		$this->load->view('common/skeleton', $data);
	}
}