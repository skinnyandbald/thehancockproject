<?php
class Email extends MY_Controller
{
	private $controller = '';

	function __construct()
	{
		parent::__construct();
		
		$this->controller = strtolower(__CLASS__);
	}
	
	function index()
	{
		
	}
	
	function preview($template = 'welcome')
	{
		$data['controller'] = $this->controller;
		$data['sub'] 		= $this->controller;
		$data['view'] 		= $this->controller.'/'.$template;
		$this->load->view('common/skeleton', $data);
	}
}