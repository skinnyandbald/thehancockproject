<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	function __construct()
    {
    	parent::__construct();
    	
    	//$this->_init_brand_fonts();
    	
    	if(ENVIRONMENT == 'local')
    	  $this->output->enable_profiler(TRUE);
    }
    
    function _init_brand_fonts()
    {
	    $this->webfonts 	= array();
		$this->webfonts[] = new Webfont('kim@zinepak.com');
		$this->webfonts[] = new Webfont('jacobyr@gmail.com');
    }
}