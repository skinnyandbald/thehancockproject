$(function()
{
	// Customize twitter feed
	var hideTwitterAttempts = 0;
	function hideTwitterBoxElements() {
	    setTimeout( function() {
	    	if ( $('#twitter-widget-0').length )
	    	{
	        	var ihead = $('#twitter-widget-0').contents().find('head');
				ihead.append("<link href='<?php echo $_GET["fontface_path"] ?>' rel='stylesheet' type='text/css'>");
	        }
	            
	        if ( $('[id*=twitter]').length )
	        {
		        $('[id*=twitter]').each( function() {
		            if ( $(this).width() == 220 ) {
		                $(this).width( 198 ); //override min-width of 220px
		            }
		            
		            var ibody = $(this).contents().find( 'body' );
		            /*ibody.width( $(this).width() + 20 ); //remove scrollbar by adding width*/
					
		            if ( ibody.find( '.timeline .stream .h-feed li.tweet' ).length ) {
		            ibody.find( '.timeline' ).css( 'border', 0 );
		            ibody.find( '.timeline .stream' ).css( 'overflow-x', 'hidden' );
		            ibody.find( '.timeline .stream' ).css( 'overflow-y', 'scroll' );
		            /*ibody.find( '.timeline-header').hide();*/
		            ibody.find( '.timeline-footer').hide();
		            ibody.find( '.e-entry-title').css('font-family', '<?php echo $_GET["fontfamily"] ?>, sans-serif').css('font-size', '<?php echo $_GET["font_size"] ?>').css('line-height', '<?php echo $_GET["line_height"] ?>').css('padding', '.5em 0em');
		            }
		            else {
		                $(this).hide();
		            }
		        });
	        }
	        hideTwitterAttempts++;
	        if ( hideTwitterAttempts < 3 )
	        {
	            hideTwitterBoxElements();
	        }
	    }, 1500);
	}
	
	// somewhere in your code after html page load
	hideTwitterBoxElements();
});
