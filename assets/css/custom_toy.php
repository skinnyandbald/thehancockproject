<?php echo header("Content-Type: text/css"); ?>

html, body {
  background: url(<?php echo $_GET['asset_url'] ?>images/bg.jpg) no-repeat;
  background-repeat:repeat;
  background-position:center top; 
  color: darkgrey;
  padding: 0;
  margin: 0;
  font-family: 'Open Sans', sans-serif;
  font-weight: 300;
  font-weight: normal;
  font-style: normal;
  line-height: 1;
  position: relative;
  cursor: default;
}
  	
.handmade_logo {
	background: url('<?php echo $_GET['asset_url'] ?>images/handmade-logo.png') no-repeat center;
	background-size: contain;
	width: 100%;
	min-width: 520px;
	height: 53px;
	margin: 2em 0em 1em 0em;
}

h4, h4 a, h4 a:active, h4 a:visited, h4 a:link {
	text-align: center;
	color: #5da9dd;
	font-size: 1em;
	text-decoration: none;
}