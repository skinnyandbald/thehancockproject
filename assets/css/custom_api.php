<?php echo header("Content-Type: text/css"); ?>

body, html {
	margin-top: 0em;
	font-size: 1.1em;
	background: #fff;
	color: #333333;
	line-height: 1.9em;
	text-align: center;
}

#webfont_preview {
	margin: 0 auto;
	background: url('<?php echo $_GET['asset_url'] ?>images/api/preview_overlay.png') #fff no-repeat;
	background-size: contain;
	max-width: 800px;
	padding: 3em 8.5em 0 2.5em;
	width: 800px;
	height: 449px;
}

.tag {
    font-weight: normal;
    text-align: right;
    font-size: 1.2em;
    line-height: 0em;
    padding-right: 1em;
    text-rendering: optimizeLegibility;
}