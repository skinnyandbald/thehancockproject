<?php echo header("Content-Type: text/css"); ?>

@import url(http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700);

body {
	background: none !important;
	color: darkgrey;
	font-weight: 400;
}

.top-bar {
	background: url('<?php echo $_GET['asset_url'] ?>images/email/bg.gif') repeat #282828;
 	background-size: 50%;
 	height:auto;
 	margin-bottom: 2em;
}

.header {
 	line-height: 3.5em !important;
 	padding: .5em 0em .35em 0em;
}

.row {
	max-width: 53em !important;
}

h2 {
	font-family: 'kimzinepakcom';
	color: #231f20;
	text-transform: capitalize;
	text-rendering: optimizeLegibility;
}

h5 {
	color: #231f20;
	text-transform: none;
}

.brand-text {
	font-family: 'jacobyrgmailcom';
	font-size: .9em;
	text-rendering: optimizeLegibility;
}

.brand-face {
	background: url('<?php echo $_GET['asset_url'] ?>images/email/hancock.png') no-repeat;
	background-size: 100%;
	width: 50px;
	height:50px;
}

.hot {
	color: #ec008c;
}

#slider {
	background: #231f20;
}

ul, ol, dl {
	list-style-position: inside;
}

.label {
	padding: .3em .5em;
}